const DIRECTIONS = {
  NORTH: 1,
  EAST: 2,
  SOUTH: 3,
  WEST: 4,
};

module.exports = {
  DIRECTIONS: DIRECTIONS,
};

export {};
