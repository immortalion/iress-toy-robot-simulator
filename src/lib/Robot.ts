const { DIRECTIONS } = require("../constants");
const { error } = require("../utils");

interface Coords {
  x: number;
  y: number;
  direction: number;
}

class Robot {
  private x: number;
  private y: number;
  private boundaries: Coords;
  private direction: number;
  protected isPlaced: boolean;
  constructor(boundaries: Coords) {
    this.x = 0;
    this.y = 0;
    this.boundaries = boundaries;
    this.direction = DIRECTIONS.NORTH;
    this.isPlaced = false;
  }

  // Places on the grid
  place(x: number, y: number, direction: number) {
    if (
      x <= this.boundaries.x &&
      x >= 0 &&
      this.y <= this.boundaries.y &&
      y >= 0 &&
      this.isValidDirection(direction)
    ) {
      this.x = x;
      this.y = y;
      this.direction = DIRECTIONS[direction];
      this.isPlaced = true;
    } else {
      return error(
        "Error: Failed to place the Robot. Please check: direction, position to be valid"
      );
    }
  }

  // Moves the robot forward based on it's current facing direction
  move() {
    switch (this.direction) {
      case DIRECTIONS.NORTH:
        this.y + 1 < this.boundaries.y ? this.y++ : null;
        break;
      case DIRECTIONS.EAST:
        this.x + 1 < this.boundaries.x ? this.x++ : null;
        break;
      case DIRECTIONS.SOUTH:
        this.y - 1 >= 0 ? this.y-- : null;
        break;
      case DIRECTIONS.WEST:
        this.x - 1 >= 0 ? this.x-- : null;
        break;
    }
  }

  // Turns 90deg to the left
  left(): number {
    return this.direction - 1 === 0
      ? (this.direction = DIRECTIONS.WEST)
      : this.direction--;
  }

  // Tunrs 90deg turn to the right
  right() {
    return this.direction + 1 === Object.keys(DIRECTIONS).length
      ? (this.direction = DIRECTIONS.NORTH)
      : this.direction++;
  }

  // Get direction name from index
  getDirection() {
    for (const direction in DIRECTIONS) {
      if (this.direction === DIRECTIONS[direction]) {
        return direction;
      }
    }
  }

  isValidDirection(direction: number) {
    return !!DIRECTIONS[direction];
  }

  report() {
    return `${this.x},${this.y},${this.getDirection()}`;
  }
}

module.exports = Robot;
