const fs = require("fs");
const path = require("path");

describe("FileReader", () => {
  let filePath: string;

  beforeEach(() => {
    filePath = path.resolve(__dirname, "../", "commands.txt");
  });

  it("should return a result by reading a file", () => {
    fs.readFileSync(filePath, (error: string, data: string) => {
      if (error) {
        throw error;
      }

      expect(data).not.toBeNull();
    });
  });

  it("should split commands into an array by reading the file contents", () => {
    fs.readFileSync(filePath, (error: string, data: string) => {
      if (error) {
        throw error;
      }

      const dataArray = data.split(/\r\n/);

      expect(dataArray).not.toBeNull();
      expect(dataArray.length).toBeGreaterThan(1);
    });
  });
});

export {};
