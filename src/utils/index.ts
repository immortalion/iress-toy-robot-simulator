const chalk = require("chalk");

const error = chalk.bold.red;
const success = chalk.bold.green;
const info = chalk.blue;

const command = {
  type: "input",
  name: "action",
  message: "Enter a command [HELP for commands]: ",
};

const message = (msg: string, indicator: Function) =>
  console.log(indicator(`\n${msg}\n`));

module.exports = {
  command,
  error: (msg: string) => message(msg, error),
  info: (msg: string) => message(msg, info),
  success: (msg: string) => message(msg, success),
  log: (msg: string, items: {}) => console.log(msg, items || ""),
  buildPlaceObject: (data: Array<string>) => {
    return {
      x: parseInt(data[1]),
      y: parseInt(data[2]),
      direction: data[3],
    };
  },
};

export {};
